Installing Quake III Arena data
===============================

Please use game-data-packager to build and install the quake3-data
or quake3-demo-data package.

For quake3-data you will need at least baseq3/pak0.pk3 (479493658 bytes)
from a Quake III Arena CD-ROM or installation.
game-data-packager can download all other necessary files. Typical uses:

    game-data-packager -d ~/Downloads quake3 /path/to/search
        Save the generated package to ~/Downloads to install later

    game-data-packager --install quake3 /path/to/search
        Install the generated package, do not save it for later

game-data-packager will automatically search various common installation
paths. If you are installing from a CD-ROM or a non-standard location,
add the installation path or CD-ROM mount point to the command line.

Add the path to linuxq3apoint-1.32b-3.x86.run to the game-data-packager
command line if you already have it available locally, to avoid
re-downloading it.

For more details please see the output of "game-data-packager quake3 --help"
or the game-data-packager(6) man page.

Quake III Team Arena expansion
==============================

Add a directory containing missionpack/pak0.pk3 to the game-data-packager
command line if you have Quake III Team Arena available.

Quake III Arena demo
====================

For quake3-demo-data (experimental) only downloadable files are required.
Typical use:

    game-data-packager --install quake3 --demo

Add the paths to linuxq3ademo-1.11-6.x86.gz.sh and
linuxq3apoint-1.32b-3.x86.run to the game-data-packager command line if
you already have them available locally. Both of those files are needed:
the retail patch is needed to modify the demo data to be compatible with
the ioquake3 engine.
